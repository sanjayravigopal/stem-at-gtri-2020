# Daily Notes 06/16/20

## Updates

### Logistics

 - App Dev Team is all on Win 10
 - App Dev Team selected Java as their dev language
 - ModSim Team is all on MacOS
 - ModSim Team selected Python as their dev language
 - Daily stand up
   - ~15 min
   - 1000hrs
 - Repo created
   - access granted
 
### App Dev

 - phone to phone BT not emulated 
 - physical devices available
   - can also fake it
 - app can distribute itself via BT
   - Wrap the APK internally
   - BT file transfer
   - Special consent required on RX phone
     - android limitation
	 - not super clean, but this is for emergencies anyway

### ModSim

 - search and min path researched
 - possible future need for dynamic adaptations 

## Tasking

### Will

 - meeting notes on-line

### App Dev

 - start with simple message dist
 - proof of concept APK dist without internet/app-store

### ModSim

 - develop static grid of nodes
   - future: add variable distance
   - future: add variable connection radius
 - add edges dynamically
